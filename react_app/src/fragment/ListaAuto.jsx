import '../css/stylea.css';
//import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./Header";
import Footer from './Footer';
import DataTable from 'react-data-table-component';
import { Autos } from '../hooks/Conexion';
import { borrarSesion } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import EditarAuto from './EditarAuto';

const ListaAuto = () => {
    const navegation = useNavigate();
    const [data, setData] = useState([]);
    const [llauto, setLlauto] = useState(false);

    const [showForm, setShowForm] = useState(false);
    const [selectedRow, setSelectedRow] = useState(null);
    const handleClose = () => {setShowForm(false); setLlauto(false);};

    const handleRowClick = (row) => {
        // Acción que deseas realizar al hacer clic en la fila
        console.log("Has hecho clic en una fila. La información de la fila es:", row.external_id);
        setSelectedRow(row.external_id);
        setShowForm(true);
    };

    if (!llauto) {
        Autos().then((info) => {
            console.log(info);
            if (info.code === 200) {
                console.log("Entro");
                setData(info.info);
                setLlauto(true);
            }
        });
    }

    const columns = [
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Anio',
            selector: row => row.anio,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Cilindraje',
            selector: row => row.cilindraje,
        },
        {
            name: 'Precio',
            selector: row => '$ ' + row.precio,
        },
        {
            name: 'Marca',
            selector: row => row.marca.nombre,
        }
    ];



    return (

        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    <h3><a className="dropdown-item text-center text-gray-500" href="#">Autos Disponibles</a></h3>
                    {/** DE AQUI CUERPO */}
                    <div className='container-fluid'>
                        <a className='btn btn-success' href={'/auto/registro'}>NUEVO</a>
                        <DataTable
                            columns={columns}
                            data={data}
                            onRowClicked={handleRowClick}
                        />
                        {showForm && (
                            <div className="model_box">
                                <Modal
                                    show={showForm}
                                    onHide={handleClose}
                                    backdrop="static"
                                    keyboard={false}
                                >
                                    <Modal.Header closeButton>
                                        <Modal.Title>Editar Auto</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <EditarAuto row={selectedRow} onCloseModal={handleClose} />
                                    </Modal.Body>

                                    <Modal.Footer>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Cancelar
                                        </Button>

                                    </Modal.Footer>
                                </Modal>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <Footer />
        </div>


    );
}

export default ListaAuto;