import '../css/stylea.css';
import Header from "./Header";
import Footer from './Footer';
import { AutosCant, MarcasCant } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import { useNavigate } from 'react-router-dom';
import mensajes from '../utilidades/Mensajes';
import { useState } from 'react';
const Inicio = () => {
    const navegation = useNavigate();
    const [nro, setNro] = useState(0);
    const [nroA, setNroA] = useState(0);
    MarcasCant(getToken()).then((info) => {
        //  console.log(info);
        if (info.code === 401 && info.msg == 'Token no valido o expirado!') {
            borrarSesion();
            mensajes(info.message);
            navegation("/sesion");
        } else {
            //console.log(info.data.length);    
            setNro(info.info);
        }
    });

    const onSubmit = (data) => {
        borrarSesion();
    };

    //autos contar
    AutosCant(getToken()).then((info) => {
        //    console.log(info);
        if (info.code === 401 && info.msg == 'Token no valido o expirado!') {
            borrarSesion();
            mensajes(info.message);
            navegation("/sesion");
        } else {
            //console.log(info);    
            setNroA(info.info);
        }
    });

    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}
                    <div className='container-fluid'>
                        <h1 className="h3 mb-0 text-gray-800">Dashboard</h1>
                        <div className="row">



                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-primary shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Nro de marcas</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nro}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-success shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Autos en existencias</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nroA}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="text-center text-lg-start mt-4 pt-2">
                                <button type="submit" className="btn btn-primary btn-lg"
                                    id="btnlogin" onClick={onSubmit} >Cerrar sesion</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default Inicio;
